import { combineReducers } from 'redux'

// import your Module reducers here and combine them
import home from './home/reducers'

export default (history) => combineReducers({
  home,
  // rest of your reducers
});