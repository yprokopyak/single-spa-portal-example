import React from 'react';
import { Switch} from 'react-router-dom';
import asyncComponent from "./common/components/AsyncComponent";
import AppliedRoute from "./common/components/AppliedRoute";

// Configure routes
const Home = asyncComponent(() => import("./home"));
const About = asyncComponent(() => import("./about"));
const PageNotFound = asyncComponent(() => import("./common/components/PageNotFound"));

export default ({ childProps }) =>
    <Switch>
        <AppliedRoute
            path="/"
            exact
            component={Home}
            props={childProps}
        />
        <AppliedRoute
            path="/about"
            exact
            component={About}
            props={childProps}
        />
        <AppliedRoute
            path="*"
            exact
            component={PageNotFound}
            props={childProps}
        />
    </Switch>
;