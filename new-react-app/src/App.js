import React, { Component } from "react";
import { Provider } from 'react-redux'
import Store, { history } from './configureStore';
import {Router } from 'react-router-dom'

// Styles
import 'bootstrap/dist/css/bootstrap.css';
import './App.krem.css';

// routes
import Routes from './routes';

// common components
import Header from './common/components/Header'
import Footer from './common/components/Footer'

class App extends Component {
  componentDidCatch (error, info) {
    this.setState({hasError: true})
  }

    render() {
    return (
      <Provider store={Store}>
        <Router history={history}>
          <div className="App">
            <Header />
            <div className="wrap">
              <Routes></Routes>
            </div>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
