/* eslint-env node */
const config = require('./webpack.config.js');
const webpack = require('webpack');
const path = require('path');

config.plugins.push(new webpack.NamedModulesPlugin());
config.plugins.push(new webpack.HotModuleReplacementPlugin());
config.devServer = {
  headers: {
    "Access-Control-Allow-Origin": "*",
  }
}

config.output = {
  filename: 'new-react-app.js',
  library: 'new-react-app',
  libraryTarget: 'amd',
  path: path.resolve(__dirname, 'build/new-react-app'),
  publicPath: "http://localhost:8240/"
}

config.mode = 'development'

module.exports = config;