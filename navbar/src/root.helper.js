export const links = [
  {
    name: 'People',
    href: '/people'
  },
  {
    name: 'Planets',
    href: '/planets'
  },
  {
    name: 'New React App',
    href: '/newapp'
  }
]
